# Use an official Node.js runtime as a parent image
FROM node:latest

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in package.json
RUN npm install

# Expose ports for the application
EXPOSE 3000


# Define volumes for OpenAPI and Postman
VOLUME [ "/app/openapi", "/app/postman" ]

# Set default command to run the three scripts sequentially
CMD ["sh", "-c", "node convert.js && sleep 10 && node merge.js && sleep 10 && node show.js"]

