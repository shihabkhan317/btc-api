const YAML = require('yamljs');
const fs = require('fs');
const path = require('path');

const directoryPath = './openapi';

const files = fs.readdirSync(directoryPath);

let merged = { 
  openapi: '3.0.0',
  info: {
    title: 'API Documentation',
    version: '1.0.0'
  },
  paths: {},
  components: {
    securitySchemes: {
      bearerAuth: {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT'
      }
    }
  }
};

files.forEach((file) => {
  const contents = YAML.load(path.join(directoryPath, file));

  const fileName = path.parse(file).name;
  for (const path in contents.paths) {
    for (const method in contents.paths[path]) {
      const operation = contents.paths[path][method];
      if (operation.tags && Array.isArray(operation.tags)) {
        operation.tags = [fileName];
      } else {
        operation.tags = [fileName];
      }
      merged.paths[path] = merged.paths[path] || {};
      merged.paths[path][method] = operation;
    }
  }
});

const yamlString = YAML.stringify(merged);
const savePath = './openapi/1';

if (!fs.existsSync(savePath)) {
  fs.mkdirSync(savePath, { recursive: true, mode: 0o777 });
}

fs.writeFileSync('./openapi/1/merged.yaml', yamlString);

console.log('OpenAPI specs merged and written to ./openapi/1/merged.yaml');
