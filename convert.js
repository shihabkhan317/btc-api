const fs = require('fs');
const path = require('path');
const postmanToOpenApi = require('postman-to-openapi');

const pmDirectoryPath = './postman';
const oaDirectoryPath = './openapi';

function compareFolderFiles(folderPath1, folderPath2) {
  const files1 = fs.readdirSync(folderPath1);
  const files2 = fs.readdirSync(folderPath2);

  if (files1.length !== files2.length) {
    return false;
  }

  const sortedFiles1 = files1.sort();
  const sortedFiles2 = files2.sort();

  for (let i = 0; i < sortedFiles1.length; i++) {
    const fileName1 = path.parse(sortedFiles1[i]).name;
    const fileName2 = path.parse(sortedFiles2[i]).name;

    if (fileName1 !== fileName2) {
      return false;
    }
  }

  return true;
}

function convertPostmanToOpenApi(postmanEx, outputOa) {
  const postmanCollection = `${pmDirectoryPath}/${postmanEx}`;
  const outputFile = `${oaDirectoryPath}/${outputOa}`;

  return postmanToOpenApi(postmanCollection, outputFile, { defaultTag: 'General' });
}

function convertPostmanToOpenApiIfFilesNotEqual() {
  const areFilesEqual = compareFolderFiles(oaDirectoryPath, pmDirectoryPath);

  if (areFilesEqual) {
    console.log('The two folders have the same files.');
  } else {
    fs.readdir(pmDirectoryPath, (err, files) => {
      if (err) {
        console.error('Error reading directory', err);
        return;
      }
    
      const fileNames = files.map(file => path.basename(file));
      fileNames.map((fileName) => {
        convertPostmanToOpenApi(fileName,fileName.replace(/\.[^/.]+$/, '')+'.yaml')
          .then(() => {
            console.log(`Successfully converted ${fileName} to OpenAPI.`);
          })
          .catch((err) => {
            console.error(`Error converting ${fileName} to OpenAPI:`, err);
          });
      });
    });
  }
}

convertPostmanToOpenApiIfFilesNotEqual();
