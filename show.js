const express = require('express');
const YAML = require('yamljs');
const swaggerUi = require('swagger-ui-express');

const app = express();
const port = 3000;

const swaggerDocument = YAML.load('./openapi/1/merged.yaml');

app.use('/', swaggerUi.serve);
app.get('/', swaggerUi.setup(swaggerDocument));

app.listen(port, () => {
  console.log(`API documentation available at http://localhost:${port}/`);
});
