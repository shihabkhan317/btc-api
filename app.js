const express = require('express');
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const fs = require('fs');
const directoryPath = './openapi';
const pmDirectoryPath = './postman';
const path = require('path');

const app = express();

const postmanToOpenApi = require('postman-to-openapi');

async function convertPostmanToOpenApi(postmanEx, outputOa) {
  const postmanCollection = './postman/'+postmanEx;
  const outputFile = './openapi/'+outputOa;

  try {
    const result = await postmanToOpenApi(postmanCollection, outputFile, { defaultTag: 'General' });
    const result2 = await postmanToOpenApi(postmanCollection, null, { defaultTag: 'General' });
  } catch (err) {
    console.log(err);
  }

  postmanToOpenApi(postmanCollection, outputFile, { defaultTag: 'General' })
    .then(result => {
      // console.log(`OpenAPI specs: ${result}`)
    })
    .catch(err => {
      console.log(err);
    });
}

function compareFolderFiles(folderPath1, folderPath2) {
  const files1 = fs.readdirSync(folderPath1);
  const files2 = fs.readdirSync(folderPath2);

  if (files1.length !== files2.length) {
    return false;
  }

  const sortedFiles1 = files1.sort();
  const sortedFiles2 = files2.sort();

  for (let i = 0; i < sortedFiles1.length; i++) {
    const fileName1 = path.parse(sortedFiles1[i]).name;
    const fileName2 = path.parse(sortedFiles2[i]).name;

    if (fileName1 !== fileName2) {
      return false;
    }
  }

  return true;
}



function convertPostmanToOpenApiIfFilesNotEqual() {
  const areFilesEqual = compareFolderFiles("./openapi", "./postman");

  if (areFilesEqual) {
    console.log('The two folders have the same files.');
  } else {
    fs.readdir(pmDirectoryPath, (err, files) => {
      if (err) {
        console.error('Error reading directory', err);
        return;
      }
    
      const fileNames = files.map(file => path.basename(file));
      fileNames.map((fileName) => {
        convertPostmanToOpenApi(fileName,fileName.replace(/\.[^/.]+$/, '')+'.yaml');
      });
    });
  }
}


process.on('startup', () => {
  console.log('Initializing application...');
  // Perform one-time initialization tasks here
});

function loadSwaggerRoutes(app) {
  return new Promise((resolve, reject) => {
    fs.readdir(directoryPath, (err, files) => {
      if (err) {
        console.log('Unable to scan directory: ' + err);
        reject(err);
        return;
      }

      files.forEach((file) => {
        app.use('/'+file.replace(/\.[^/.]+$/, ''), swaggerUi.serve, swaggerUi.setup(YAML.load('./openapi/'+file)));
      });

      resolve();
    });
  });
}

loadSwaggerRoutes(app)
  .then(() => {
    app.get('/', (req, res) => {
      convertPostmanToOpenApiIfFilesNotEqual();
      fs.readdir(directoryPath, (err, files) => {
        if (err) {
          res.status(500).send('Error reading directory');
          return;
        }

        const fileNames = files.map(file => path.basename(file));
        const listItems = fileNames.map(fileName => `
          <li>
            <a href="${fileName.replace(/\.[^/.]+$/, '')}">${fileName.replace(/\.[^/.]+$/, '')}</a>
          </li>
        `);

        const html = `
          <html>
            <body>
              <ul>
                ${listItems.join('')}
              </ul>
            </body>
          </html>
        `;

        res.send(html);
      });
    });
  })
  .catch((err) => {
    console.error('Error loading swagger routes:', err);
  });



app.listen(3000, () => {
  console.log('Server started on http://localhost:3000');
});
