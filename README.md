# btc api > Postman export to Open API Documentation

## Getting started

```
git clone https://gitlab.com/vishnusoman/btc-api.git
cd btc-api
npm i
node convert.js
node merge.js
node show.js
open browser > http://localhost:3000
```

## Docker

```
clone vishnusomantechversant/btc form hub
docker build -t btc-api .
docker run -d -p 3000:3000 -v D:\BTC\btc-api\openapi:/app/openapi -v D:\BTC\btc-api\postman:/app/postman btc-api
open browser > http://localhost:3000
```

## Note

Add postman export with clear name(it need to use as url path) to postman folder.
